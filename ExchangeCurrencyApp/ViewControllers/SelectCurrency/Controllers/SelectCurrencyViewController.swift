//
//  SelectCurrencyViewController.swift
//  ExchangeCurrencyApp
//
//  Created by Виктор Сирик on 08.11.2021.
//

import UIKit
import Alamofire
class SelectCurrencyViewController: UIViewController {

    @IBOutlet weak var currencyFirstPickerView: UIPickerView!
    @IBOutlet weak var currencySecondPickerView: UIPickerView!
    @IBOutlet weak var enterValueTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    var arrayOfCurrency: [String] = []
    var selectFrom: String = ""
    var selectTo: String = ""
    var q: Double = 0
    let apiKey = "57fb1b76a9msh46392529f8fdcebp1b2bc0jsn05cc081835ca"
    let apiHost = "currency-exchange.p.rapidapi.com"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currencyFirstPickerView.dataSource = self
        currencyFirstPickerView.delegate = self
        
        currencySecondPickerView.dataSource = self
        currencySecondPickerView.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadCurrency()
    }
    
    
    @IBAction func exchangeRatesAction(_ sender: Any) {
        if let number = enterValueTextField.text, !number.isEmpty, (Int(number) != nil) {
            q = Double(number)!
            loadResult()
        } else {
            resultLabel.text = "Incorrect data"
        }
    }
    
}
