//
//  SelectCurrencyViewController + Extensions.swift
//  ExchangeCurrencyApp
//
//  Created by Виктор Сирик on 08.11.2021.
//

import Foundation
import UIKit
import Alamofire
extension SelectCurrencyViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayOfCurrency.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == currencyFirstPickerView {
            return arrayOfCurrency[row]
        } else if pickerView == currencySecondPickerView {
            return arrayOfCurrency[row]
        } else {
            return nil
        }
    }

    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == currencyFirstPickerView {
            selectFrom = arrayOfCurrency[row]
        } else if pickerView == currencySecondPickerView {
            selectTo = arrayOfCurrency[row]
        }
        
    }
    
     func loadCurrency(){
         let urlPath = "https://currency-exchange.p.rapidapi.com/listquotes"
         let dataRest = RestData().loadInfo(urlPath: urlPath, completionHandler:{ (response: [String]?, error: String?) -> Void in
            guard error == nil else{
                debugPrint("error")
                return
            }
            
             guard let data = response else {
                debugPrint("error")
                return
            }
            
            self.arrayOfCurrency = data
            
            DispatchQueue.main.async {
             self.currencySecondPickerView.reloadAllComponents()
             self.currencyFirstPickerView.reloadAllComponents()
             }
        })
    }
    
    func loadResult() {
        let urlPath = "https://currency-exchange.p.rapidapi.com/exchange"
        let parameters: [String: String] = ["from": selectFrom, "to": selectTo, "q": String(q)]
        let dataRest = RestData().loadResult(urlPath: urlPath, parameters: parameters, completionHandler:{ (response: Double?, error: String?) -> Void in
            guard error == nil else{
                debugPrint("error")
                return
            }
            
            guard let data = response else {
                debugPrint("errorData")
                return
            }
            
            self.resultLabel.text = "Result: \(data)"
        })
    }
                                               
}
