//
//  NetworkManager.swift
//  ExchangeCurrencyApp
//
//  Created by Виктор Сирик on 10.11.2021.
//

import Foundation
import Alamofire

class NetworkManager{
    let apiKey = "57fb1b76a9msh46392529f8fdcebp1b2bc0jsn05cc081835ca"
    let apiHost = "currency-exchange.p.rapidapi.com"
    
    static let shared = NetworkManager()
    
    func loadData(urlPath: String,
                  method: HTTPMethod,
                  parameters: [String: Any]? = nil,
                  completionHandler: @escaping(Data?, String?) -> Void) {
        
    

        let httpHeaders: [String: String] = ["x-rapidapi-host": apiHost, "x-rapidapi-key": apiKey]
        
        AF.request(urlPath,
                   method: method,
                   parameters: parameters,
                   headers: HTTPHeaders(httpHeaders)).response { response in
            
            switch response.result {
            case .success(let data):
                completionHandler(data, nil)
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        }
        
    }
    
 
}
