//
//  RestData.swift
//  ExchangeCurrencyApp
//
//  Created by Виктор Сирик on 10.11.2021.
//

import Foundation

class RestData {
    let apiKey = "57fb1b76a9msh46392529f8fdcebp1b2bc0jsn05cc081835ca"
    let apiHost = "currency-exchange.p.rapidapi.com"
    
    func loadInfo<T: Codable>(urlPath: String, parameters: [String: Any]? = nil, completionHandler: @escaping ([T]?, String?) -> Void) {

        NetworkManager.shared.loadData(urlPath: urlPath, method: .get, parameters: parameters){ data, error in
            
            guard error == nil else{
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else{
                completionHandler(nil, nil)
                return
            }
            
            do{
                let decoder = JSONDecoder()
                let response = try decoder.decode([T].self, from: data)
                completionHandler(response, nil)
            } catch let errorRes as NSError{
                completionHandler(nil, errorRes.localizedDescription)
            }
        }
    }
    
    func loadResult<T: Codable>(urlPath: String, parameters: [String: Any]? = nil, completionHandler: @escaping (T?, String?) -> Void) {

        NetworkManager.shared.loadData(urlPath: urlPath, method: .get, parameters: parameters){ data, error in
            
            guard error == nil else{
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else{
                completionHandler(nil, nil)
                return
            }
            
            do{
                let decoder = JSONDecoder()
                let response = try decoder.decode(T.self, from: data)
                completionHandler(response, nil)
            } catch let errorRes as NSError{
                completionHandler(nil, errorRes.localizedDescription)
            }
        }
    }
    
}
